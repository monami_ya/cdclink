//
//	0000_0000 番地 限定 のベクターテーブル.
//	=====================================================================
//	概要:
//	・RAM上の 0x1fff_0000番地(256byte) にユーザー用の例外テーブルを用意。
//
//	・この実装はRAM上の例外テーブルを使用する間接Jumpの実装になっている。
//	・この実装は０番地スタートのファームウェア(BOOTLOADER)のみに必要。
//


//	注釈:
//	・STM32 の NVIC 相当の割り込みコントローラーには、CPU例外テーブルの
//	　ベースアドレスを変更する機能があるので、このような実装は不要である。
//
//	通常使用するベクターテーブルはここではなく、 crt0.c に記述されている.
//		(0x0000_0100 番地に配置され、0x1fff_0000番地にコピーされる)
//

#include "vector.h"

#if	_ROMADRS

//
//	リセットベクター.
//
void SetSoftwareResetVector(int *vect)
{
	// dummy.
}
	// ---------------------------------------------------
	// 0000_4000番地にビルドする場合は不要.
	// ---------------------------------------------------

#else

	//
	// ---------------------------------------------------
	// 0000_0000番地にビルドする場合は以下のように,
	// RAMベクターへ飛ぶ余分なテーブルが追加される.
	// ---------------------------------------------------
	//


#include "monitor/gpio.h"
#define	REBOOT_FUNCTION	(1)	// cdctool/cdcbootからユーザープログラムをBOOTする機能を実装.


// 電源投入時、Port P39 が GND接地されていたらユーザープログラムをBOOTしたいなら定義.
//
#define	USER_BOOT_FUNCTION	Yes

#define	USER_BOOT_JUMPER	P39			// ジャンパーの指定.
#define	USER_BOOT_VECTOR	0x4000		// ユーザープログラムの指定.


#define	FM3_RAMADRS_LO		0x1fff0000	// RAM実装番地
#define	FM3_RAMADRS_HI		0x20010000
#define	FM3_ROMADRS_LO		0x00000000	// ROM実装番地
#define	FM3_ROMADRS_HI		0x00100000


//;
//;		C Runtime Startup for FM3:MB9B618T
//;
EXTERN		_estack			;
//;
//;		割り込みベクトル:
//;
	__attribute__ ((section(".isr_vector"))) void (*const 
g_pfnVectors						[])(void) = {
	LPVOID  &_estack	       	 ,/*00 Top of Stack */
	DCD     j_Reset_Handler      ,/*01 Reset Handler */
	DCD     j_NMI_Handler        ,/*02 NMI Handler */
	DCD     j_HardFault_Handler  ,/*03 Hard Fault Handler */
	DCD     j_MemManage_Handler  ,/*04 MPU Fault Handler */
	DCD     j_BusFault_Handler   ,/*05 Bus Fault Handler */
	DCD     j_UsageFault_Handler ,/*06 Usage Fault Handler */
	DCD     j_Handler07          ,/*07 Reserved */
	DCD     j_Handler08          ,/*08 Reserved */
	DCD     j_Handler09          ,/*09 Reserved */
	DCD     j_Handler10          ,/*10 Reserved */
	DCD     j_SVC_Handler        ,/*11 SVCall Handler */
	DCD     j_DebugMon_Handler   ,/*12 Debug Monitor Handler */
	DCD     j_Handler13          ,/*13 Reserved */
	DCD     j_PendSV_Handler     ,/*14 PendSV Handler */
	DCD     j_SysTick_Handler    ,/*15 SysTick Handler */
	DCD     j_CSV_Handler        ,/*16 0: Clock Super Visor */
	DCD     j_SWDT_Handler       ,/*17 1: Software Watchdog Timer */
	DCD     j_LVD_Handler        ,/*18 2: Low Voltage Detector */
	DCD     j_MFT_WG_IRQHandler  ,/*19 3: Wave Form Generator / DTIF */
	DCD     j_INT0_7_Handler     ,/*20 4: External Interrupt Request ch.0 to ch.7 */
	DCD     j_INT8_15_Handler    ,/*21 5: External Interrupt Request ch.8 to ch.15 */
	DCD     j_DT_Handler         ,/*22 6: Dual Timer / Quad Decoder */
	DCD     j_MFS0RX_IRQHandler  ,/*23 7: MultiFunction Serial ch.0 */
	DCD     j_MFS0TX_IRQHandler  ,/*24 8: MultiFunction Serial ch.0 */
	DCD     j_MFS1RX_IRQHandler  ,/*25 9: MultiFunction Serial ch.1 */
	DCD     j_MFS1TX_IRQHandler  ,/*26 10: MultiFunction Serial ch.1 */
	DCD     j_MFS2RX_IRQHandler  ,/*27 11: MultiFunction Serial ch.2 */
	DCD     j_MFS2TX_IRQHandler  ,/*28 12: MultiFunction Serial ch.2 */
	DCD     j_MFS3RX_IRQHandler  ,/*29 13: MultiFunction Serial ch.3 */
	DCD     j_MFS3TX_IRQHandler  ,/*30 14: MultiFunction Serial ch.3 */
	DCD     j_MFS4RX_IRQHandler  ,/*31 15: MultiFunction Serial ch.4 */
	DCD     j_MFS4TX_IRQHandler  ,/*32 16: MultiFunction Serial ch.4 */
	DCD     j_MFS5RX_IRQHandler  ,/*33 17: MultiFunction Serial ch.5 */
	DCD     j_MFS5TX_IRQHandler  ,/*34 18: MultiFunction Serial ch.5 */
	DCD     j_MFS6RX_IRQHandler  ,/*35 19: MultiFunction Serial ch.6 */
	DCD     j_MFS6TX_IRQHandler  ,/*36 20: MultiFunction Serial ch.6 */
	DCD     j_MFS7RX_IRQHandler  ,/*37 21: MultiFunction Serial ch.7 */
	DCD     j_MFS7TX_IRQHandler  ,/*38 22: MultiFunction Serial ch.7 */
	DCD     j_PPG_Handler        ,/*39 23: PPG */
	DCD     j_TIM_IRQHandler     ,/*40 24: OSC / PLL / Watch Counter */
	DCD     j_ADC0_IRQHandler    ,/*41 25: ADC0 */
	DCD     j_ADC1_IRQHandler    ,/*42 26: ADC1 */
	DCD     j_ADC2_IRQHandler    ,/*43 27: ADC2 */
	DCD     j_MFT_FRT_IRQHandler ,/*44 28: Free-run Timer */
	DCD     j_MFT_IPC_IRQHandler ,/*45 29: Input Capture */
	DCD     j_MFT_OPC_IRQHandler ,/*46 30: Output Compare */
	DCD     j_BT_IRQHandler      ,/*47 31: Base Timer ch.0 to ch.7 */
	DCD     j_CAN0_IRQHandler    ,/*48 32: CAN ch.0 */
	DCD     j_CAN1_IRQHandler    ,/*49 33: CAN ch.1 */
	DCD     j_USBF_Handler       ,/*50 34: USB Function */
	DCD     j_USB_Handler        ,/*51 35: USB Function / USB HOST */
	DCD     j_Handler52 	     ,/*52 36: Reserved */
	DCD     j_Handler53     	 ,/*53 37: Reserved */
	DCD     j_DMAC0_Handler      ,/*54 38: DMAC ch.0 */
	DCD     j_DMAC1_Handler      ,/*55 39: DMAC ch.1 */
	DCD     j_DMAC2_Handler      ,/*56 40: DMAC ch.2 */
	DCD     j_DMAC3_Handler      ,/*57 41: DMAC ch.3 */
	DCD     j_DMAC4_Handler      ,/*58 42: DMAC ch.4 */
	DCD     j_DMAC5_Handler      ,/*59 43: DMAC ch.5 */
	DCD     j_DMAC6_Handler      ,/*60 44: DMAC ch.6 */
	DCD     j_DMAC7_Handler      ,/*61 45: DMAC ch.7 */
	DCD     j_Handler62     	 ,/*62 46: Reserved */
	DCD     j_Handler63 	     ,/*63 67: Reserved */
};


	// ---------------------------------------------------
	// 0000_0000番地にビルドする場合は
	// Software Reset 直後かどうかを判定して、
	// 別のアプリケーションを起動するコードも追加される.
	// ---------------------------------------------------
	//
#define	MARK0	0x55aa0f00
#define	MARK1	0x55aa0f01
#define	MARK2	0x55aa0f02

static int software_Reset_State[3]={0,0,0};
static int *software_Reset_New_Vector=0;

//void 	*g_Vectors[64];

void	Reset_Handler(void) ;
#define	ROM_VECTOR	(uint*) 0x00000100
#define	RAM_VECTOR	(uint*) 0x1fff0000

void
j_Reset_Handler			(void) { uint *s,*t;int i;


#if	REBOOT_FUNCTION		// cdctool/cdcbootからユーザープログラムをBOOTする機能を実装.
	if(	(software_Reset_State[0]==MARK0 ) &&
		(software_Reset_State[1]==MARK1 ) &&
		(software_Reset_State[2]==MARK2 )) {

		// Software Reset 直後と判定.
		software_Reset_State[0]=0;
		void (*func)(void);
		func = (void (*)()) software_Reset_New_Vector[1];	// SP , PC ,...
		// 飛ぶ.
		func();
	}
#endif

#ifdef	USER_BOOT_FUNCTION 	// Port P39 が GND接地されていたらユーザープログラムをBOOT.

	// BootJumperをPULLUPする.
	pinModePullup(USER_BOOT_JUMPER,1);
	
	//
	// 4000番地のユーザーベクターを見る.
	//
	int *user_vector_p = (int*) USER_BOOT_VECTOR;
	int user_sp = user_vector_p[0];
	int user_pc = user_vector_p[1];

	//
	// 4000番地のユーザーベクターが正しいかどうか確認する.
	//
	if(( user_sp >= FM3_RAMADRS_LO )
	 &&( user_sp <  FM3_RAMADRS_HI )
	 &&( user_pc >= USER_BOOT_VECTOR)
	 &&( user_pc <  FM3_ROMADRS_HI)  ){
		if( digitalRead(USER_BOOT_JUMPER)==0 ) {
			void (*func)(void);
			//
			// 4000番地のユーザーベクターをRAMにコピー.
			//
			s = USER_BOOT_VECTOR;
			t = RAM_VECTOR;
			for(i=0;i<64;i++) {
				*t++ = *s++;
			}
			func = (void (*)()) (USER_BOOT_VECTOR+4);	// Initial PC.
			//
			// 4000番地のユーザーベクターへ飛ぶ.
			//
			func();
		}
	}
#endif

//	s = g_Vectors;
	s = ROM_VECTOR;
	t = RAM_VECTOR;

//;
//;	g_pfnVectors[64] をRAMにコピーする.
//;
	for(i=0;i<64;i++) {
		*t++ = *s++;
	}
	Reset_Handler();
}

//
//	ソフトウェアリセットを実行する直前にアプリケーションのベクターテーブル
//	を設定する.
//
void SetSoftwareResetVector(int *vect)
{
	software_Reset_State[0]=MARK0;
	software_Reset_State[1]=MARK1;
	software_Reset_State[2]=MARK2;

	software_Reset_New_Vector = vect;
}


#endif	// _ROMADRS
