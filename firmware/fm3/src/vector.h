#ifndef	vector_h_
#define	vector_h_

#define	      VECTOR(f)		void f(void);
#define	 WEAK_VECTOR(f)		void f(void) __attribute__ ((weak));
#define	ALIAS_VECTOR(f)		void f(void) __attribute__ ((weak, alias("Default_Handler")));

//各例外ベクターのプロトタイプ宣言をここで行なっておく:

VECTOR(		j_Reset_Handler   	)
VECTOR(		j_NMI_Handler     	)
VECTOR(		j_HardFault_Handler	)
VECTOR(		j_MemManage_Handler	)
VECTOR(		j_BusFault_Handler	)
VECTOR(		j_UsageFault_Handler	)
VECTOR(		j_Handler07      	)
VECTOR(		j_Handler08      	)
VECTOR(		j_Handler09      	)
VECTOR(		j_Handler10      	)
VECTOR(		j_SVC_Handler			)
VECTOR(		j_DebugMon_Handler	)
VECTOR(		j_Handler13      	)
VECTOR(		j_PendSV_Handler  	)
VECTOR(		j_SysTick_Handler 	)
VECTOR(		j_Default_Handler 	)
VECTOR(		j_CSV_Handler	       	)
VECTOR(		j_SWDT_Handler       	)
VECTOR(		j_LVD_Handler        	)
VECTOR(		j_MFT_WG_IRQHandler  	)
VECTOR(		j_INT0_7_Handler     	)
VECTOR(		j_INT8_15_Handler    	)
VECTOR(		j_DT_Handler         	)
VECTOR(		j_MFS0RX_IRQHandler  	)
VECTOR(		j_MFS0TX_IRQHandler  	)
VECTOR(		j_MFS1RX_IRQHandler  	)
VECTOR(		j_MFS1TX_IRQHandler  	)
VECTOR(		j_MFS2RX_IRQHandler  	)
VECTOR(		j_MFS2TX_IRQHandler  	)
VECTOR(		j_MFS3RX_IRQHandler  	)
VECTOR(		j_MFS3TX_IRQHandler  	)
VECTOR(		j_MFS4RX_IRQHandler  	)
VECTOR(		j_MFS4TX_IRQHandler  	)
VECTOR(		j_MFS5RX_IRQHandler  	)
VECTOR(		j_MFS5TX_IRQHandler  	)
VECTOR(		j_MFS6RX_IRQHandler  	)
VECTOR(		j_MFS6TX_IRQHandler  	)
VECTOR(		j_MFS7RX_IRQHandler  	)
VECTOR(		j_MFS7TX_IRQHandler  	)
VECTOR(		j_PPG_Handler        	)
VECTOR(		j_TIM_IRQHandler     	)
VECTOR(		j_ADC0_IRQHandler    	)
VECTOR(		j_ADC1_IRQHandler    	)
VECTOR(		j_ADC2_IRQHandler    	)
VECTOR(		j_MFT_FRT_IRQHandler 	)
VECTOR(		j_MFT_IPC_IRQHandler 	)
VECTOR(		j_MFT_OPC_IRQHandler 	)
VECTOR(		j_BT_IRQHandler      	)
VECTOR(		j_CAN0_IRQHandler    	)
VECTOR(		j_CAN1_IRQHandler    	)
VECTOR(		j_USBF_Handler       	)
VECTOR(		j_USB_Handler        	)
VECTOR(		j_Handler52      	)
VECTOR(		j_Handler53      	)
VECTOR(		j_DMAC0_Handler      	)
VECTOR(		j_DMAC1_Handler      	)
VECTOR(		j_DMAC2_Handler      	)
VECTOR(		j_DMAC3_Handler      	)
VECTOR(		j_DMAC4_Handler      	)
VECTOR(		j_DMAC5_Handler      	)
VECTOR(		j_DMAC6_Handler      	)
VECTOR(		j_DMAC7_Handler      	)
VECTOR(		j_Handler62      	)
VECTOR(		j_Handler63      	)
//	アセンブラ風味で書くためのマクロ.

#define	uint	unsigned int
#define	EXTERN	extern uint
#define	LPVOID  (void*)
#define	HANDLER(f)	void f(void) {  while(1){}  };
#define	DCD	

extern	int	main();


#endif
