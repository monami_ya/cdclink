//
//	�O�Ԓn�̃x�N�^�[����P�e�e�e�Q�O�O�O�O�Ԓn�̃x�N�^�[�ɔ�ԏ���.
//

.set ramvect,		0x1FFF0000

	.global j_NMI_Handler     	
	.global j_HardFault_Handler	
	.global j_MemManage_Handler	
	.global j_BusFault_Handler	
	.global j_UsageFault_Handler
	.global j_Handler07
	.global j_Handler08
	.global j_Handler09
	.global j_Handler10
	.global j_SVC_Handler		
	.global j_DebugMon_Handler	
	.global j_Handler13
	.global j_PendSV_Handler  	
	.global j_SysTick_Handler 	
	.global j_Default_Handler 	
	.global j_CSV_Handler	    
	.global j_SWDT_Handler      
	.global j_LVD_Handler       
	.global j_MFT_WG_IRQHandler 
	.global j_INT0_7_Handler    
	.global j_INT8_15_Handler   
	.global j_DT_Handler        
	.global j_MFS0RX_IRQHandler 
	.global j_MFS0TX_IRQHandler 
	.global j_MFS1RX_IRQHandler 
	.global j_MFS1TX_IRQHandler 
	.global j_MFS2RX_IRQHandler 
	.global j_MFS2TX_IRQHandler 
	.global j_MFS3RX_IRQHandler 
	.global j_MFS3TX_IRQHandler 
	.global j_MFS4RX_IRQHandler 
	.global j_MFS4TX_IRQHandler 
	.global j_MFS5RX_IRQHandler 
	.global j_MFS5TX_IRQHandler 
	.global j_MFS6RX_IRQHandler 
	.global j_MFS6TX_IRQHandler 
	.global j_MFS7RX_IRQHandler 
	.global j_MFS7TX_IRQHandler 
	.global j_PPG_Handler       
	.global j_TIM_IRQHandler    
	.global j_ADC0_IRQHandler   
	.global j_ADC1_IRQHandler   
	.global j_ADC2_IRQHandler   
	.global j_MFT_FRT_IRQHandler
	.global j_MFT_IPC_IRQHandler
	.global j_MFT_OPC_IRQHandler
	.global j_BT_IRQHandler     
	.global j_CAN0_IRQHandler   
	.global j_CAN1_IRQHandler   
	.global j_USBF_Handler      
	.global j_USB_Handler       
	.global j_Handler52
	.global j_Handler53
	.global j_DMAC0_Handler     
	.global j_DMAC1_Handler     
	.global j_DMAC2_Handler     
	.global j_DMAC3_Handler     
	.global j_DMAC4_Handler     
	.global j_DMAC5_Handler     
	.global j_DMAC6_Handler     
	.global j_DMAC7_Handler     
	.global j_Handler62
	.global j_Handler63



	.text
	.thumb

	.thumb_func 
j_NMI_Handler     	:
	mov		r3,#0x08
	b		ramjump

	.thumb_func 
j_HardFault_Handler	:
	mov		r3,#0x0c
	b		ramjump

	.thumb_func 
j_MemManage_Handler	:
	mov		r3,#0x10
	b		ramjump

	.thumb_func 
j_BusFault_Handler	:
	mov		r3,#0x14
	b		ramjump

	.thumb_func 
j_UsageFault_Handler:
	mov		r3,#0x18
	b		ramjump

	.thumb_func 
j_Handler07:
	mov		r3,#0x1c
	b		ramjump

	.thumb_func 
j_Handler08:
	mov		r3,#0x20
	b		ramjump

	.thumb_func 
j_Handler09:
	mov		r3,#0x24
	b		ramjump

	.thumb_func 
j_Handler10:
	mov		r3,#0x28
	b		ramjump

	.thumb_func 
j_SVC_Handler		:
	mov		r3,#0x2c
	b		ramjump

	.thumb_func 
j_DebugMon_Handler	:
	mov		r3,#0x30
	b		ramjump

	.thumb_func 
j_Handler13:
	mov		r3,#0x34
	b		ramjump

	.thumb_func 
j_PendSV_Handler  	:
	mov		r3,#0x38
	b		ramjump

	.thumb_func 
j_SysTick_Handler 	:
	mov		r3,#0x3c
	b		ramjump

	.thumb_func 
j_CSV_Handler	    :
	mov		r3,#0x40
	b		ramjump

	.thumb_func 
j_SWDT_Handler      :
	mov		r3,#0x44
	b		ramjump

	.thumb_func 
j_LVD_Handler       :
	mov		r3,#0x48
	b		ramjump

	.thumb_func 
j_MFT_WG_IRQHandler :
	mov		r3,#0x4c
	b		ramjump

	.thumb_func 
j_INT0_7_Handler    :
	mov		r3,#0x50
	b		ramjump

	.thumb_func 
j_INT8_15_Handler   :
	mov		r3,#0x54
	b		ramjump

	.thumb_func 
j_DT_Handler        :
	mov		r3,#0x58
	b		ramjump

	.thumb_func 
j_MFS0RX_IRQHandler :
	mov		r3,#0x5c
	b		ramjump

	.thumb_func 
j_MFS0TX_IRQHandler :
	mov		r3,#0x60
	b		ramjump

	.thumb_func 
j_MFS1RX_IRQHandler :
	mov		r3,#0x64
	b		ramjump

	.thumb_func 
j_MFS1TX_IRQHandler :
	mov		r3,#0x68
	b		ramjump

	.thumb_func 
j_MFS2RX_IRQHandler :
	mov		r3,#0x6c
	b		ramjump

	.thumb_func 
j_MFS2TX_IRQHandler :
	mov		r3,#0x70
	b		ramjump

	.thumb_func 
j_MFS3RX_IRQHandler :
	mov		r3,#0x74
	b		ramjump

	.thumb_func 
j_MFS3TX_IRQHandler :
	mov		r3,#0x78
	b		ramjump

	.thumb_func 
j_MFS4RX_IRQHandler :
	mov		r3,#0x7c
	b		ramjump

	.thumb_func 
j_MFS4TX_IRQHandler :
	mov		r3,#0x80
	b		ramjump

	.thumb_func 
j_MFS5RX_IRQHandler :
	mov		r3,#0x84
	b		ramjump

	.thumb_func 
j_MFS5TX_IRQHandler :
	mov		r3,#0x88
	b		ramjump

	.thumb_func 
j_MFS6RX_IRQHandler :
	mov		r3,#0x8c
	b		ramjump

	.thumb_func 
j_MFS6TX_IRQHandler :
	mov		r3,#0x90
	b		ramjump

	.thumb_func 
j_MFS7RX_IRQHandler :
	mov		r3,#0x94
	b		ramjump

	.thumb_func 
j_MFS7TX_IRQHandler :
	mov		r3,#0x98
	b		ramjump

	.thumb_func 
j_PPG_Handler       :
	mov		r3,#0x9c
	b		ramjump

	.thumb_func 
j_TIM_IRQHandler    :
	mov		r3,#0xa0
	b		ramjump

	.thumb_func 
j_ADC0_IRQHandler   :
	mov		r3,#0xa4
	b		ramjump

	.thumb_func 
j_ADC1_IRQHandler   :
	mov		r3,#0xa8
	b		ramjump

	.thumb_func 
j_ADC2_IRQHandler   :
	mov		r3,#0xac
	b		ramjump

	.thumb_func 
j_MFT_FRT_IRQHandler:
	mov		r3,#0xb0
	b		ramjump

	.thumb_func 
j_MFT_IPC_IRQHandler:
	mov		r3,#0xb4
	b		ramjump

	.thumb_func 
j_MFT_OPC_IRQHandler:
	mov		r3,#0xb8
	b		ramjump

	.thumb_func 
j_BT_IRQHandler     :
	mov		r3,#0xbc
	b		ramjump

	.thumb_func 
j_CAN0_IRQHandler   :
	mov		r3,#0xc0
	b		ramjump

	.thumb_func 
j_CAN1_IRQHandler   :
	mov		r3,#0xc4
	b		ramjump

	.thumb_func 
j_USBF_Handler      :
	mov		r3,#0xc8
	b		ramjump

	.thumb_func 
j_USB_Handler       :
	mov		r3,#0xcc
	b		ramjump

	.thumb_func 
j_Handler52:
	mov		r3,#0xd0
	b		ramjump

	.thumb_func 
j_Handler53:
	mov		r3,#0xd4
	b		ramjump

	.thumb_func 
j_DMAC0_Handler     :
	mov		r3,#0xd8
	b		ramjump

	.thumb_func 
j_DMAC1_Handler     :
	mov		r3,#0xdc
	b		ramjump

	.thumb_func 
j_DMAC2_Handler     :
	mov		r3,#0xe0
	b		ramjump

	.thumb_func 
j_DMAC3_Handler     :
	mov		r3,#0xe4
	b		ramjump

	.thumb_func 
j_DMAC4_Handler     :
	mov		r3,#0xe8
	b		ramjump

	.thumb_func 
j_DMAC5_Handler     :
	mov		r3,#0xec
	b		ramjump

	.thumb_func 
j_DMAC6_Handler     :
	mov		r3,#0xf0
	b		ramjump

	.thumb_func 
j_DMAC7_Handler     :
	mov		r3,#0xf4
	b		ramjump

	.thumb_func 
j_Handler62:
	mov		r3,#0xf8
	b		ramjump

	.thumb_func 
j_Handler63:
	mov		r3,#0xfc
//	b		ramjump


ramjump:
	ldr		r2,=ramvect

	.thumb_func 
	ldr		r2,[r2,r3]
	bx		r2


//
//
//[EOF]
