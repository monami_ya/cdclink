#ifndef	_sampling_h_
#define	_sampling_h_

#include "utype.h"

typedef struct {
//	--------------------------------------------------
	uchar  cmd;			//				SAMPLING
	uchar  subcmd;		// = a/d;		a=1 d=2 stop=0
	ushort size;		// = count;		1040
	uint   rate;		// = rate;		1000(sample/sec)
//	--------------------------------------------------
	uchar  channels;	//	ch(s)  = channels(1..16) 1(chs)
	uchar  trig_mode;	//				0(freerun)
	uchar  trig_ch;		//				0(ch)
	uchar  port_sel;	//				0(port)
	uint   reserve;
//	--------------------------------------------------
} SAMPLE_PARAM;

enum {
	ADC_STOP,
	ADC_ANALOG,
	ADC_DIGITAL,
};

int	sample_init(SAMPLE_PARAM *param);	//サンプリング開始.
int	sample_stop();						//サンプリング停止.
int	sample_read(char *buf,int size);	//サンプリングデータ取得.


#endif
