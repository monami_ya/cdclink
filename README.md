# Firmwares for CDCLink.

CDCLink is a JTAG pod framework designed by Koji Yamamoto (twitter id: @iruka3).

This repository contains CDCLink specified firmwares.

It is required firmware and control agent if you want to use. But here is no control agent.
You can get openocd with CDCLink support at http://support.monami-ya.com/openocd/.

# For more information about firmwares

Read firmware/ReadMe.txt. It's written in Japanese. We are welcome to contribute translation.
